import random

def main():
  print("Probability of last ball being azure: " + str(probability(10)))
  print("Probability of last ball being azure: " + str(probability(50)))
  print("Probability of last ball being azure: " + str(probability(90)))

def probability(k):
  count = 0
  for i in range(2000):
    count += one_trial(k)
  
  print("Initial azure count: " + str(k))
  print("Initial carmine count: " + str(100 - k))
  print("Azure count: " + str(count) + "\tTotal: " + str(2000))

  return count / 2000
    
def one_trial(k):
  count = 0
  balls = list()
  for i in range(k):
    balls.append("azure")
  for i in range(100 - k):
    balls.append("carmine")
  
  init_ball = None
  while len(balls) != 0:
    if (init_ball == None):
      rand_int = 0
      if (len(balls) > 1):
        rand_int = random.randint(0, len(balls) - 1)
      init_ball = balls.pop(rand_int)

    if len(balls) == 0:
      if new_ball == "azure":
        return 1
      else:
        return 0
    
    rand_int = 0
    if (len(balls) > 1):
      rand_int = random.randint(0, len(balls) - 1)
    new_ball = balls[rand_int]

    if new_ball == init_ball:
      balls.pop(rand_int)
      if len(balls) == 0:
        if new_ball == "azure":
          return 1
        else:
          return 0
    else:
      init_ball = None
  
  return 0

if (__name__ == "__main__"):
  main()
